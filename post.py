from libs.poster_bot import poster_bot
import sys
import settings

if len(sys.argv) != 3:
    print("insufficient arguments")
    sys.exit(1)

bot = poster_bot()
bot.sendBook(type=sys.argv[1], chat_id=sys.argv[2])