import requests as rq
import pymysql as mysql
import re
import settings

class poster_bot:
    __token = settings.TOKEN
    __url = "https://api.telegram.org/bot"
    __conn = None
    __cur  = None
    __symbols = ("абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ",
               "abvgdeejzijklmnoprstufhzcss_y_euaABVGDEEJZIJKLMNOPRSTUFHZCSS_Y_EUA")
    __tr = None

    def __init__(self):
        self.__url += self.__token
        self.__conn = mysql.connect(host=settings.MYSQL_HOST,
                             user=settings.MYSQL_USER,
                             password=settings.MYSQL_PASS,
                             db=settings.MYSQL_DB,
                             charset="utf8")
        self.__cur = self.__conn.cursor()
        self.__tr  = {ord(a):ord(b) for a, b in zip(*self.__symbols)}

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__cur.close()
        self.__conn.close()

    def translit (self, txt):
        return txt.translate(self.__tr)

    def sendBook(self, chat_id=settings.DEFAULT_CHANNEL_NAME, type=settings.DEFAULT_BOOK_TYPE):

        strsql = """select b.id,
                 b.book_title,
                 b.book_link,
                 b.book_download,
                 b.book_author,
                 b.book_description,
                 b.book_image,
                 b.book_year,
                 b.content_length,
                 b.last_modified,
                 b.file_extension 
                 from books b, categories c
                 where c.type = %(type)s 
                 and b.category_id = c.public_id 
                 and b.content_length <= 52428800 
                 and not b.file_extension in ('rar', 'zip', 'txt') 
                 and b.is_shared = 0 
                 order by rand() 
                 limit 2"""

        if self.__cur.execute(strsql, {"type":type}):
            for id, book_title, book_link, book_download, book_author, book_description, book_image, book_year, content_length, last_modified, extension, in self.__cur:
                document = book_download

                caption = book_title + (", " + book_author if book_author else "")
                #tags = ' '.join(["#" + re.sub(r"[^a-zA-Z\u0400-\u0500]+", "", tag) for tag in caption.split(" ") if len(re.sub(r"[^a-zA-Z\u0400-\u0500]+", "", tag)) > 1])
                caption +=  ", " + book_year if book_year else ""
                #caption += "\n#psychology #психология #selfimprovement #саморазвити"
                url = self.__url + "sendDocument"

                translit = self.translit(book_title + " " + (book_author if book_author else "")) + ("_" + book_year if book_year else "")
                translit = re.sub(r"[^a-zA-Z0-9 \-_]+", "", translit)

                r = rq.get(document)

                print(r.headers['Content-Length'], content_length, translit)

                if int(r.headers['Content-Length']) != int(content_length):
                    continue

                r = rq.post(url, data={"chat_id":chat_id,"caption":caption}, files={"document":(translit + "." + extension, r.content)})

                if r.status_code != 200:
                    print("Error book: ", r.content)
                    continue
                else:
                    print("Book posted: ", r.content)

                if book_image:
                    caption = book_description[:197] + "..." if book_description else ""

                    url = self.__url + "sendPhoto"
                    r = rq.post(url, data={"chat_id": chat_id, "caption": caption, "photo": book_image})
                    if r.status_code != 200:
                        print("Error photo: ", r.content)
                    else:
                        print("Photo posted: ", r.content)


                strsql = "update books set is_shared = 1 where id = %(id)s"
                conn = mysql.connect(host="localhost",
                                     user="root",
                                     password="root",
                                     db="padabum_books",
                                     charset="utf8")
                cur = conn.cursor()
                cur.execute(strsql, {"id":id})
                conn.commit()
                cur.close()
                conn.close()