import requests as rq
from lxml import html
import pymysql as mysql
from datetime import datetime
import time

class crawler:
    __base = "http://padabum.com/"
    __conn = None
    __cur = None
    __book_num = 0

    def __init__(self):
        self.__conn = mysql.connect(host="localhost",
                                           user="root",
                                           password="root",
                                           db="padabum_books",
                                           charset="utf8")
        self.__cur = self.__conn.cursor()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__conn.close()
        self.__cur.close()

    def collect_categories (self):
        r = rq.get(self.__base)
        dom = html.fromstring(r.content)
        folders = dom.xpath('//*[@class="folder"]')

        for folder in folders:
            public_id = folder.xpath('@href')[0].replace("/?id=", "")
            name = folder.xpath('text()')[0]
            strsql = "insert into categories (public_id,name) values(%(public_id)s, %(name)s)"
            self.__cur.execute(strsql, {'public_id':public_id,'name':name})
            self.__conn.commit()

    def collect_books(self, type):
        strsql = "select public_id from categories where type = %(type)s"
        res = self.__cur.execute(strsql, {"type":type})
        if res:
            for public_id, in self.__cur:
                self.parse_books(public_id)

    def parse_books(self, public_id, start=0, pg=1):
        r = rq.get(self.__base + "?id=" + str(public_id) + "&start=" + str(start))
        if r.status_code != 200:
            return
        dom = html.fromstring(r.content)
        lnks = dom.xpath('//table[@class="books-list"]//a[@class="download"]')
        for lnk in lnks:
            self.get_book(lnk.xpath('@href')[0][1:], public_id)

        pages = dom.xpath('//a[@style="font-size:14px;"]')
        for page in pages:
            page_num = page.xpath('text()')[0]
            start = page.xpath('@href')[0].replace("/index.php?id=" + str(public_id) + "&start=", "")
            if int(page_num) < pg:
                continue

            self.parse_books(public_id, start, int(page_num))


    def get_book(self, link, category_id):
        book_link = self.__base + link
        r = rq.get(book_link)
        if r.status_code != 200:
            return

        dom = html.fromstring(r.content)

        book_download = dom.xpath('//a[@class="download_book"]//@href')
        if len(book_download) != 1 or str(book_download[0]).startswith("x.php?id=") == False:
            return

        book_download = self.__base + book_download[0].strip()

        h = rq.get(book_download, stream=True)
        if h.headers['Content-Type'] != 'application/octet-stream':
            return


        d = datetime.strptime(h.headers['Last-Modified'], "%a, %d %b %Y %H:%M:%S %Z")
        last_modified = str(time.mktime(d.timetuple()))

        filename = h.headers['Content-Disposition'].split('"')[-2]
        extension = filename.split('.')[-1]

        book_title = dom.xpath('//*[@id="book_title"]//text()')
        book_author = dom.xpath('//*[@id="book_author"]//text()')

        if len(book_title) == 1:
            book_title = book_title[0].strip()
        else:
            book_title = None

        if len(book_author) == 1:
            book_author = book_author[0].strip()
        else:
            book_author = None

        book_image = dom.xpath('//table[@class="book-view-content"]//tr[position()=2]//td[position()=1]//img//@src')
        if len(book_image) == 1:
            book_image = self.__base + book_image[0].strip()[1:]
        else:
            book_image = None


        trs = dom.xpath('//table[@class="book-view-content"]//tr[position()=2]//td[position()=2]//table//tr')
        for tr in trs:
            title = tr.xpath('td[position()=1]//text()')[0].strip()
            if title == "Автор":
                book_author = tr.xpath('td[position()=2]//text()')[0].strip()
            if title == "Год":
                book_year = tr.xpath('td[position()=2]//text()')[0].strip()
            else:
                book_year = None

            if title == "Размер":
                book_size = tr.xpath('td[position()=2]//text()')[0].strip()
            else:
                book_size = None

        book_description = dom.xpath('//*[@class="book-description"]//text()')
        if len(book_description) == 1:
            book_description = book_description[0].strip()
        else:
            book_description = None

        self.__book_num += 1
        print(self.__book_num, book_title, book_author, book_year, book_size, book_link)

        #return

        conn = mysql.connect(host="localhost",
                                    user="root",
                                    password="root",
                                    db="padabum_books",
                                    charset="utf8")
        cur = conn.cursor()


        strsql = "insert into books (book_title, " \
                 "book_link, " \
                 "book_download, " \
                 "book_author, " \
                 "book_description, " \
                 "book_image, " \
                 "book_size, " \
                 "book_year, " \
                 "file_extension," \
                 "content_type," \
                 "content_length," \
                 "last_modified," \
                 "category_id) " \
                 "values(%(book_title)s," \
                 "%(book_link)s," \
                 "%(book_download)s," \
                 "%(book_author)s," \
                 "%(book_description)s," \
                 "%(book_image)s," \
                 "%(book_size)s," \
                 "%(book_year)s," \
                 "%(file_extension)s," \
                 "%(content_type)s," \
                 "%(content_length)s," \
                 "%(last_modified)s," \
                 "%(category_id)s)"

        try:
            cur.execute(strsql, {
                "book_title": book_title,
                "book_link": book_link,
                "book_download": book_download,
                "book_author": book_author,
                "book_description": book_description,
                "book_image": book_image,
                "book_size": book_size,
                "book_year": book_year,
                "category_id": category_id,
                "file_extension": extension,
                "content_type": h.headers['Content-Type'],
                "content_length": h.headers['Content-Length'],
                "last_modified": last_modified
            })
            conn.commit()
        except Exception as err:
            print(err)
        finally:
            cur.close()
            conn.close()