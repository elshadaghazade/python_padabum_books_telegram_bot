from dotenv import load_dotenv
import os

load_dotenv()

TOKEN = os.getenv("TOKEN")
MYSQL_HOST = os.getenv("MYSQL_HOST")
MYSQL_USER = os.getenv("MYSQL_USER")
MYSQL_PASS = os.getenv("MYSQL_PASS")
MYSQL_DB = os.getenv("MYSQL_DB")

DEFAULT_CHANNEL_NAME = os.getenv("DEFAULT_CHANNEL_NAME")
DEFAULT_BOOK_TYPE = os.getenv("DEFAULT_BOOK_TYPE")